unit untErrorObject;

interface

type
  TErrorObject = class
    private
      FStatus: Boolean;
      FMessage: String;
      FOperation: String;
      constructor Create;
      destructor Destroy;
    public
      property Status: Boolean read FStatus write FStatus;
      property Message: String read FMessage write FMessage;
      property Operation: String read FOperation write FOperation;
  end;

implementation

{ TErrorObject }

constructor TErrorObject.Create;
begin
  inherited;
end;

destructor TErrorObject.Destroy;
begin
  inherited;
end;

end.
