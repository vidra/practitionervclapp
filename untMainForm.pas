unit untMainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ToolWin, Vcl.ActnMan, Vcl.ActnCtrls, Vcl.Menus
  , untDMMain
  , untPractitioner
  , untObserverInterfaces
  , untErrorObject
  ;

type
  TPractObserver = class(TInterfacedObject, IPractitionerObserver)
  private
    { private declarations }
    edtName: TEdit;
    edtMail: TEdit;
    edtPractId: TEdit;
    edtGiven: TEdit;
    cbActive: TCheckBox;

  protected
    { protected declarations }
  public
    { public declarations }
    procedure Update(NewPractitioner: TPractitioner);
    constructor Create(Mail, Name, Given, PractId: TEdit; Active: TCheckBox);
    destructor Free;

  published
    { published declarations }
  end;


type
  TfrmMainForm = class(TForm)
    pnlDataSearch: TPanel;
    pnlOperations: TPanel;
    pnlPractitioner: TPanel;
    DBGridPractitioner: TDBGrid;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Splitter3: TSplitter;
    btnClose: TButton;
    menuPractitioner: TMainMenu;
    miFile: TMenuItem;
    miFHIRServer: TMenuItem;
    Practitioner1: TMenuItem;
    Close1: TMenuItem;
    btnSearchPractitioner: TButton;
    SearchPractitioner: TMenuItem;
    PickServer1: TMenuItem;
    lblPrefix: TLabel;
    lblGiven: TLabel;
    lblName: TLabel;
    lblPractId: TLabel;
    cbActive: TCheckBox;
    edtPrefix: TEdit;
    edtGiven: TEdit;
    edtName: TEdit;
    edtPractId: TEdit;
    gbPractitionerSearch: TGroupBox;
    lblParamName: TLabel;
    lblParamId: TLabel;
    lblParamEmail: TLabel;
    edtParamName: TEdit;
    edtParamEmail: TEdit;
    edtParamId: TEdit;
    cbParamActive: TCheckBox;
    SavePractitioner: TMenuItem;
    SaveDialog1: TSaveDialog;
    lblEmail: TLabel;
    edtMail: TEdit;
    Patient1: TMenuItem;
    SearchPatient: TMenuItem;
    SavePatient: TMenuItem;
    procedure btnCloseClick(Sender: TObject);
    procedure btnSearchPractitionerClick(Sender: TObject);
    procedure PickServer1Click(Sender: TObject);
    procedure Search1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SavePractitionerClick(Sender: TObject);
    procedure DBGridPractitionerTitleClick(Column: TColumn);
    procedure SearchPatientClick(Sender: TObject);
    procedure SavePatientClick(Sender: TObject);
  private
    MyServer: String;
    MyPractPanel: TPractObserver;
    procedure ClearPractitionerPanel;
    procedure SetFormCaption;
    procedure CloseAll;
    procedure SaveJSONTextToFile(ResourceName: String);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMainForm: TfrmMainForm;

implementation

uses
  untFrmServer
  , untFrmPatient
  ;

{$R *.dfm}

procedure TfrmMainForm.btnCloseClick(Sender: TObject);
begin
  CloseAll;
end;

procedure TfrmMainForm.btnSearchPractitionerClick(Sender: TObject);
var
  old_cursor: TCursor;
  myErrorObj: TErrorObject;
begin
  myErrorObj := TErrorObject.Create;
  old_cursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  ClearPractitionerPanel;
  try

    if not DMMain.GetPractitionerList(edtParamEmail.Text
                                        , edtParamName.Text
                                        , edtParamId.Text
                                        , cbActive.Checked
                                        , myErrorObj) then
      MessageDlg('Query data from server in operation ' + myErrorObj.Operation + ' returned error ' + myErrorObj.Message
                  , mtError
                  , [mbOk]
                  , 0);

  finally
    Screen.Cursor := old_cursor;
    myErrorObj.Free;
  end;
end;

procedure TfrmMainForm.ClearPractitionerPanel;
begin
  edtName.Text := '';
  edtGiven.Text := '';
  edtPrefix.Text := '';
  edtPractId.Text := '';
  cbActive.Checked := False;
end;

procedure TfrmMainForm.SetFormCaption;
begin
  Self.Caption := 'FHIR VCL Practitioner ' + DMMain.GetWorkingServer;
end;

procedure TfrmMainForm.CloseAll;
begin
  DMMain.ObserverPractitionerServer.UnRegisterObserver(MyPractPanel);
  Close;
end;

procedure TfrmMainForm.SaveJSONTextToFile(ResourceName: String);
var
  sl: TStringList;
  myRes: String;
begin
  myRes := dmMain.ReturnJsonTextFromResource(ResourceName);
  if  myRes <> '' then
  begin
    SaveDialog1.Execute;
    if SaveDialog1.FileName <> '' then
    begin
      sl := TStringList.Create;
      try
        sl.Add(myRes);
        sl.SaveToFile(SaveDialog1.FileName);
        sl.Clear;
      finally
        sl.Free;
      end;
    end;
  end
  else
    MessageDlg(ResourceName + ' JSON object is empty. Use Search to get data', mtInformation, [mbOk], 0);
end;

procedure TfrmMainForm.DBGridPractitionerTitleClick(Column: TColumn);
{$J+}
  const PreviousColumnIndex : Integer = -1;
{$J-}
begin
  if PreviousColumnIndex >-1 then
  begin
    DBGridPractitioner.Columns[PreviousColumnIndex].title.Font.Style :=
    DBGridPractitioner.Columns[PreviousColumnIndex].title.Font.Style - [fsBold];
  end;
  Column.title.Font.Style := Column.title.Font.Style + [fsBold];
  PreviousColumnIndex := Column.Index;

  DMMain.SortGridListMemTable(Column.FieldName, DBGridPractitioner.DataSource);
end;

procedure TfrmMainForm.PickServer1Click(Sender: TObject);
begin
  FrmPickServer := TFrmPickServer.Create(Self);
  FrmPickServer.MyServer := Self.MyServer;
  try
    if FrmPickServer.ShowModal = mrOk then
    begin
      ClearPractitionerPanel;
      // http://test.fhir.org/r3/Practitioner
      dmMain.SetServerAddress(FrmPickServer.MyServer);
      SetFormCaption;
    end;
  finally
    FreeAndNil(FrmPickServer);
  end;
end;

procedure TfrmMainForm.SavePatientClick(Sender: TObject);
begin
  SaveJSONTextToFile('Patient');
end;

procedure TfrmMainForm.SavePractitionerClick(Sender: TObject);
begin
  SaveJSONTextToFile('Practitioner');
end;

procedure TfrmMainForm.Search1Click(Sender: TObject);
begin
  btnSearchPractitioner.Click;
end;

procedure TfrmMainForm.SearchPatientClick(Sender: TObject);
begin
  FrmPatient := TFrmPatient.Create(Self);
  try
    FrmPatient.ShowModal;
  finally
    FreeAndNil(FrmPatient);
  end;
end;

procedure TfrmMainForm.FormShow(Sender: TObject);
begin
  SetFormCaption;
  MyPractPanel := TPractObserver.Create(edtPrefix, edtName, edtGiven, edtPractId, cbActive);
  DMMain.ObserverPractitionerServer.RegisterObserver(MyPractPanel);
end;

{ TMyObject }

constructor TPractObserver.Create(Mail, Name, Given, PractId: TEdit;
  Active: TCheckBox);
begin
  edtMail := Mail;
  edtName := Name;
  edtgiven := Given;
  edtPractId := PractId;
  cbActive := Active;
end;

destructor TPractObserver.Free;
begin
  ;
end;

procedure TPractObserver.Update(NewPractitioner: TPractitioner);
begin
  edtName.Text := NewPractitioner.Name;
  edtGiven.Text := NewPractitioner.Given;
  edtMail.Text := NewPractitioner.Mail;
  edtPractId.Text := NewPractitioner.PractId;
  cbActive.Checked := NewPractitioner.Active;
end;

end.
