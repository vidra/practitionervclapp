unit untObserverInterfaces;

interface

uses
  untPractitioner
  , untPatient
  ;

type
  IPractitionerObserver = Interface
    // procedure Update(Podatak: String);
    procedure Update(NewPractitioner: TPractitioner);
  end;

type
  IPractitionerObserverServer = Interface
    procedure RegisterObserver(o:IPractitionerObserver);
    procedure UnRegisterObserver(o:IPractitionerObserver);
    // procedure NotifyObeservers(Podatak: String);
    procedure NotifyObeservers(NewPractitioner: TPractitioner);
  end;

type
  IPatientObserver = Interface
    procedure Update(NewPatient: TPatient);
  end;

type
  IPatientObserverServer = Interface
    procedure RegisterObserver(o:IPatientObserver);
    procedure UnRegisterObserver(o:IPatientObserver);
    procedure NotifyObeservers(NewPatient: TPatient);
  end;

implementation

end.
