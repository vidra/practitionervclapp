object FrmPatient: TFrmPatient
  Left = 0
  Top = 0
  Caption = 'Patient'
  ClientHeight = 549
  ClientWidth = 724
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 411
    Top = 156
    Height = 336
    Align = alRight
    ExplicitLeft = 632
    ExplicitTop = -263
    ExplicitHeight = 562
  end
  object Splitter3: TSplitter
    Left = 0
    Top = 153
    Width = 724
    Height = 3
    Cursor = crVSplit
    Align = alTop
    ExplicitLeft = -492
    ExplicitWidth = 1127
  end
  object DBGridPatient: TDBGrid
    Left = 0
    Top = 156
    Width = 411
    Height = 336
    Align = alClient
    DataSource = dmMain.dsPatientList
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnTitleClick = DBGridPatientTitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'PatientId'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Name'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Given'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Prefix'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Active'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EMail'
        Title.Caption = 'E-Mail'
        Visible = True
      end>
  end
  object pnlPatientData: TPanel
    Left = 414
    Top = 156
    Width = 310
    Height = 336
    Align = alRight
    TabOrder = 1
    object lblPrefix: TLabel
      Left = 20
      Top = 23
      Width = 24
      Height = 13
      Caption = 'Title:'
    end
    object lblGiven: TLabel
      Left = 20
      Top = 46
      Width = 31
      Height = 13
      Caption = 'Name:'
    end
    object lblName: TLabel
      Left = 20
      Top = 69
      Width = 46
      Height = 13
      Caption = 'Surname:'
    end
    object lblPractId: TLabel
      Left = 20
      Top = 92
      Width = 41
      Height = 13
      Caption = 'FHIR Id:'
    end
    object lblEmail: TLabel
      Left = 20
      Top = 115
      Width = 74
      Height = 13
      Caption = 'E-Mail Address:'
    end
    object cbActive: TCheckBox
      Left = 20
      Top = 138
      Width = 97
      Height = 17
      Caption = 'Active'
      TabOrder = 0
    end
    object edtPrefix: TEdit
      Left = 144
      Top = 20
      Width = 50
      Height = 21
      TabOrder = 1
    end
    object edtGiven: TEdit
      Left = 144
      Top = 43
      Width = 160
      Height = 21
      TabOrder = 2
    end
    object edtName: TEdit
      Left = 144
      Top = 66
      Width = 160
      Height = 21
      TabOrder = 3
    end
    object edtPractId: TEdit
      Left = 144
      Top = 89
      Width = 160
      Height = 21
      TabOrder = 4
    end
    object edtMail: TEdit
      Left = 144
      Top = 112
      Width = 160
      Height = 21
      TabOrder = 5
    end
  end
  object pnlDataSearch: TPanel
    Left = 0
    Top = 0
    Width = 724
    Height = 153
    Align = alTop
    BevelInner = bvLowered
    TabOrder = 2
    object btnSearchPractitioner: TButton
      Left = 335
      Top = 28
      Width = 75
      Height = 25
      Caption = 'Search'
      TabOrder = 0
      OnClick = btnSearchPatientClick
    end
    object gbPractitionerSearch: TGroupBox
      Left = 8
      Top = 8
      Width = 321
      Height = 139
      Caption = 'Parameters'
      TabOrder = 1
      object lblParamName: TLabel
        Left = 15
        Top = 25
        Width = 68
        Height = 13
        Caption = 'Patient Name:'
      end
      object lblParamId: TLabel
        Left = 15
        Top = 71
        Width = 51
        Height = 13
        Caption = 'Patient Id:'
      end
      object lblParamEmail: TLabel
        Left = 15
        Top = 48
        Width = 59
        Height = 13
        Caption = 'Patient Mail:'
      end
      object edtParamName: TEdit
        Left = 128
        Top = 22
        Width = 161
        Height = 21
        TabOrder = 0
      end
      object edtParamEmail: TEdit
        Left = 128
        Top = 45
        Width = 161
        Height = 21
        TabOrder = 1
      end
      object edtParamId: TEdit
        Left = 128
        Top = 68
        Width = 161
        Height = 21
        TabOrder = 2
      end
      object cbParamActive: TCheckBox
        Left = 128
        Top = 94
        Width = 97
        Height = 17
        Caption = 'Active'
        Checked = True
        State = cbChecked
        TabOrder = 3
      end
    end
  end
  object pnlOperations: TPanel
    Left = 0
    Top = 492
    Width = 724
    Height = 57
    Align = alBottom
    BevelInner = bvLowered
    TabOrder = 3
    object btnClose: TButton
      Left = 1040
      Top = 14
      Width = 75
      Height = 25
      Caption = 'Close'
      TabOrder = 0
    end
    object Button1: TButton
      Left = 633
      Top = 14
      Width = 75
      Height = 25
      Caption = 'Close'
      TabOrder = 1
      OnClick = Button1Click
    end
  end
end
