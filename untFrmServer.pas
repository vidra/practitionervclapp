unit untFrmServer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFrmPickServer = class(TForm)
    cbServerList: TComboBox;
    btnSave: TButton;
    btnCancel: TButton;
    pnlControls: TPanel;
    pnlOperations: TPanel;
    lblServerName: TLabel;
    procedure btnCancelClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure cbServerListExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FMyServer: String;
  public
    { Public declarations }
    property MyServer: String read FMyServer write FMyServer;
  end;

var
  FrmPickServer: TFrmPickServer;

implementation

{$R *.dfm}

procedure TFrmPickServer.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFrmPickServer.btnSaveClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFrmPickServer.cbServerListExit(Sender: TObject);
begin
  MyServer := cbServerList.Items[cbServerList.ItemIndex];
end;

procedure TFrmPickServer.FormShow(Sender: TObject);
begin
  cbServerList.ItemIndex := 0;
  if cbServerList.Items.IndexOf(MyServer) > -1 then
    cbServerList.ItemIndex := cbServerList.Items.IndexOf(MyServer)
  else
    MyServer := '';
end;

end.
