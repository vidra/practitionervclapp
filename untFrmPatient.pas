unit untFrmPatient;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Grids, Vcl.DBGrids
  , untDMMain
  , untErrorObject
  , untObserverInterfaces
  , untPatient
  ;

type
  TPatientObserver = class(TInterfacedObject, IPatientObserver)
  private
    { private declarations }
    edtName: TEdit;
    edtMail: TEdit;
    edtPractId: TEdit;
    edtGiven: TEdit;
    cbActive: TCheckBox;
  protected
    { protected declarations }
  public
    { public declarations }
    procedure Update(NewPatient: TPatient);
    constructor Create(Mail, Name, Given, PractId: TEdit; Active: TCheckBox);
    destructor Free;

  published
    { published declarations }
  end;

type
  TFrmPatient = class(TForm)
    DBGridPatient: TDBGrid;
    pnlPatientData: TPanel;
    lblPrefix: TLabel;
    lblGiven: TLabel;
    lblName: TLabel;
    lblPractId: TLabel;
    lblEmail: TLabel;
    cbActive: TCheckBox;
    edtPrefix: TEdit;
    edtGiven: TEdit;
    edtName: TEdit;
    edtPractId: TEdit;
    edtMail: TEdit;
    Splitter1: TSplitter;
    pnlDataSearch: TPanel;
    btnSearchPractitioner: TButton;
    gbPractitionerSearch: TGroupBox;
    lblParamName: TLabel;
    lblParamId: TLabel;
    lblParamEmail: TLabel;
    edtParamName: TEdit;
    edtParamEmail: TEdit;
    edtParamId: TEdit;
    cbParamActive: TCheckBox;
    Splitter3: TSplitter;
    pnlOperations: TPanel;
    btnClose: TButton;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure btnSearchPatientClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure DBGridPatientTitleClick(Column: TColumn);
  private
    { Private declarations }
    MyPatientPanel: TPatientObserver;
    procedure ClearPatientPanel;
  public
    { Public declarations }
  end;

var
  FrmPatient: TFrmPatient;

implementation

{$R *.dfm}

procedure TFrmPatient.btnSearchPatientClick(Sender: TObject);
var
  old_cursor: TCursor;
  myErrorObj: TErrorObject;
begin
  myErrorObj := TErrorObject.Create;
  old_cursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  ClearPatientPanel;
  try

    if not DMMain.GetPatientList(edtParamEmail.Text
                                        , edtParamName.Text
                                        , edtParamId.Text
                                        , cbActive.Checked
                                        , myErrorObj) then
      MessageDlg('Query data from server in operation ' + myErrorObj.Operation + ' returned error ' + myErrorObj.Message
                  , mtError
                  , [mbOk]
                  , 0);

  finally
    Screen.Cursor := old_cursor;
    myErrorObj.Free;
  end;
end;

procedure TFrmPatient.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TFrmPatient.ClearPatientPanel;
begin
  edtName.Text := '';
  edtGiven.Text := '';
  edtPrefix.Text := '';
  edtPractId.Text := '';
  cbActive.Checked := False;
end;

procedure TFrmPatient.DBGridPatientTitleClick(Column: TColumn);
{$J+}
  const PreviousColumnIndex : Integer = -1;
{$J-}
begin
  if PreviousColumnIndex >-1 then
  begin
    DBGridPatient.Columns[PreviousColumnIndex].title.Font.Style :=
    DBGridPatient.Columns[PreviousColumnIndex].title.Font.Style - [fsBold];
  end;
  Column.title.Font.Style := Column.title.Font.Style + [fsBold];
  PreviousColumnIndex := Column.Index;

  DMMain.SortGridListMemTable(Column.FieldName, DBGridPatient.DataSource);
end;

procedure TFrmPatient.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DMMain.ObserverPatientServer.UnRegisterObserver(MyPatientPanel);
  Close;
end;

procedure TFrmPatient.FormShow(Sender: TObject);
begin
  MyPatientPanel := TPatientObserver.Create(edtPrefix, edtName, edtGiven, edtPractId, cbActive);
  DMMain.ObserverPatientServer.RegisterObserver(MyPatientPanel);
end;

{ TPatientObserver }

constructor TPatientObserver.Create(Mail, Name, Given, PractId: TEdit;
  Active: TCheckBox);
begin
  edtMail := Mail;
  edtName := Name;
  edtgiven := Given;
  edtPractId := PractId;
  cbActive := Active;
end;

destructor TPatientObserver.Free;
begin
  ;
end;

procedure TPatientObserver.Update(NewPatient: TPatient);
begin
  edtName.Text := NewPatient.Name;
  edtGiven.Text := NewPatient.Given;
  edtMail.Text := NewPatient.Mail;
  edtPractId.Text := NewPatient.PatientId;
  cbActive.Checked := NewPatient.Active;
end;

end.
