object dmMain: TdmMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 296
  Width = 532
  object RESTClientPractitioner: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    BaseURL = 'http://test.fhir.org/r3/Practitioner'
    Params = <>
    HandleRedirects = True
    RaiseExceptionOn500 = False
    Left = 24
    Top = 16
  end
  object RESTRequestPractitioner: TRESTRequest
    Client = RESTClientPractitioner
    Params = <>
    Response = RESTResponsePractitioner
    SynchronizedEvents = False
    Left = 61
    Top = 16
  end
  object RESTResponsePractitioner: TRESTResponse
    Left = 95
    Top = 16
  end
  object RESTResponsePractitionerAdapter: TRESTResponseDataSetAdapter
    Dataset = memtblPractitioner
    FieldDefs = <>
    Response = RESTResponsePractitioner
    Left = 128
    Top = 16
  end
  object memtblPractitioner: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 24
    Top = 62
  end
  object dsPractitioner: TDataSource
    DataSet = memtblPractitioner
    Left = 95
    Top = 62
  end
  object memtblPractitionerList: TFDMemTable
    AfterScroll = memtblPractitionerListAfterScroll
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 24
    Top = 102
    object memtblPractitionerListName: TStringField
      FieldName = 'Name'
    end
    object memtblPractitionerListGiven: TStringField
      FieldName = 'Given'
    end
    object memtblPractitionerListPrefix: TStringField
      FieldName = 'Prefix'
    end
    object memtblPractitionerListActive: TBooleanField
      FieldName = 'Active'
    end
    object memtblPractitionerListPractId: TStringField
      FieldName = 'PractId'
    end
    object memtblPractitionerListEMail: TStringField
      FieldName = 'EMail'
    end
  end
  object dsPractitionerList: TDataSource
    DataSet = memtblPractitionerList
    Left = 95
    Top = 102
  end
  object RESTClientPatient: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    BaseURL = 'http://hapi.fhir.org/baseR4/Patient'
    Params = <>
    HandleRedirects = True
    RaiseExceptionOn500 = False
    Left = 240
    Top = 16
  end
  object RESTRequestPatient: TRESTRequest
    Client = RESTClientPatient
    Params = <>
    Response = RESTResponsePatient
    SynchronizedEvents = False
    Left = 280
    Top = 16
  end
  object RESTResponsePatient: TRESTResponse
    ContentType = 'application/fhir+json'
    Left = 320
    Top = 16
  end
  object memtblPatientList: TFDMemTable
    AfterScroll = memtblPatientListAfterScroll
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 240
    Top = 64
    object memtblPatientListName: TStringField
      FieldName = 'Name'
    end
    object memtblPatientListGiven: TStringField
      FieldName = 'Given'
    end
    object memtblPatientListPrefix: TStringField
      FieldName = 'Prefix'
    end
    object memtblPatientListActive: TBooleanField
      FieldName = 'Active'
    end
    object memtblPatientListPractId: TStringField
      FieldName = 'PatientId'
    end
    object memtblPatientListEMail: TStringField
      FieldName = 'EMail'
    end
  end
  object dsPatientList: TDataSource
    DataSet = memtblPatientList
    Left = 320
    Top = 64
  end
end
