program PractitionerVCLApp;

uses
  Vcl.Forms,
  untMainForm in 'untMainForm.pas' {frmMainForm},
  untDMMain in 'untDMMain.pas' {dmMain: TDataModule},
  untFrmServer in 'untFrmServer.pas' {FrmPickServer},
  untObserverInterfaces in 'untObserverInterfaces.pas',
  untObserverObjects in 'untObserverObjects.pas',
  untErrorObject in 'untErrorObject.pas',
  untPractitioner in 'untPractitioner.pas',
  untPatient in 'untPatient.pas',
  untFrmPatient in 'untFrmPatient.pas' {FrmPatient};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMainForm, frmMainForm);
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TFrmPatient, FrmPatient);
  Application.Run;
end.
