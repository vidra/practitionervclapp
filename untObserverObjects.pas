unit untObserverObjects;

interface

uses untObserverInterfaces
    , Generics.Collections
    , StdCtrls
    , untPractitioner
    , untPatient
    ;

type
  TPractitionerObserver = class(TInterfacedObject, IPractitionerObserverServer)
  private
    { private declarations }
    Lista: TList<IPractitionerObserver>;
  protected
    { protected declarations }
  public
    { public declarations }
    constructor Create;
    destructor Free;
    procedure RegisterObserver(o:IPractitionerObserver);
    procedure UnRegisterObserver(o:IPractitionerObserver);
    procedure NotifyObeservers(NewPractitioner: TPractitioner);
  published
    { published declarations }
  end;

type
  TPatientObserver = class(TInterfacedObject, IPatientObserverServer)
  private
    { private declarations }
    Lista: TList<IPatientObserver>;
  protected
    { protected declarations }
  public
    { public declarations }
    constructor Create;
    destructor Free;
    procedure RegisterObserver(o:IPatientObserver);
    procedure UnRegisterObserver(o:IPatientObserver);
    procedure NotifyObeservers(NewPatient: TPatient);
  published
    { published declarations }
  end;

implementation

{ TObserver }

constructor TPractitionerObserver.Create;
begin
  Lista := TList<IPractitionerObserver>.Create;
end;

destructor TPractitionerObserver.Free;
begin
  Lista.Clear;
  Lista.Free;
end;

procedure TPractitionerObserver.NotifyObeservers(NewPractitioner: TPractitioner);
var
  n: Integer;
begin

  for n := 0 to Lista.Count - 1 do
    Lista.Items[n].Update(NewPractitioner);

end;

procedure TPractitionerObserver.RegisterObserver(o: IPractitionerObserver);
begin

  if not Lista.Contains(o) then
    Lista.Add(o);
end;

procedure TPractitionerObserver.UnRegisterObserver(o: IPractitionerObserver);
begin

  if Lista.Contains(o) then
    Lista.Remove(o);
end;

{ TPatientObserver }

constructor TPatientObserver.Create;
begin
  Lista := TList<IPatientObserver>.Create;
end;

destructor TPatientObserver.Free;
begin
  Lista.Clear;
  Lista.Free;
end;

procedure TPatientObserver.NotifyObeservers(NewPatient: TPatient);
var
  n: Integer;
begin

  for n := 0 to Lista.Count - 1 do
    Lista.Items[n].Update(NewPatient);

end;

procedure TPatientObserver.RegisterObserver(o: IPatientObserver);
begin
  if not Lista.Contains(o) then
    Lista.Add(o);
end;

procedure TPatientObserver.UnRegisterObserver(o: IPatientObserver);
begin
  if Lista.Contains(o) then
    Lista.Remove(o);
end;

end.
