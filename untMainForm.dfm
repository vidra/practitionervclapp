object frmMainForm: TfrmMainForm
  Left = 0
  Top = 0
  Caption = 'FHIR VCL Practitioner'
  ClientHeight = 777
  ClientWidth = 1127
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = menuPractitioner
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter3: TSplitter
    Left = 0
    Top = 153
    Width = 1127
    Height = 3
    Cursor = crVSplit
    Align = alTop
    ExplicitLeft = 1
    ExplicitTop = 1
    ExplicitWidth = 567
  end
  object pnlDataSearch: TPanel
    Left = 0
    Top = 0
    Width = 1127
    Height = 153
    Align = alTop
    BevelInner = bvLowered
    TabOrder = 0
    object btnSearchPractitioner: TButton
      Left = 335
      Top = 28
      Width = 75
      Height = 25
      Caption = 'Search'
      TabOrder = 0
      OnClick = btnSearchPractitionerClick
    end
    object gbPractitionerSearch: TGroupBox
      Left = 8
      Top = 8
      Width = 321
      Height = 139
      Caption = 'Parameters'
      TabOrder = 1
      object lblParamName: TLabel
        Left = 15
        Top = 25
        Width = 89
        Height = 13
        Caption = 'Practitioner Name:'
      end
      object lblParamId: TLabel
        Left = 15
        Top = 71
        Width = 72
        Height = 13
        Caption = 'Practitioner Id:'
      end
      object lblParamEmail: TLabel
        Left = 15
        Top = 48
        Width = 80
        Height = 13
        Caption = 'Practitioner Mail:'
      end
      object edtParamName: TEdit
        Left = 128
        Top = 22
        Width = 161
        Height = 21
        TabOrder = 0
      end
      object edtParamEmail: TEdit
        Left = 128
        Top = 45
        Width = 161
        Height = 21
        TabOrder = 1
      end
      object edtParamId: TEdit
        Left = 128
        Top = 68
        Width = 161
        Height = 21
        TabOrder = 2
      end
      object cbParamActive: TCheckBox
        Left = 128
        Top = 94
        Width = 97
        Height = 17
        Caption = 'Active'
        Checked = True
        State = cbChecked
        TabOrder = 3
      end
    end
  end
  object pnlOperations: TPanel
    Left = 0
    Top = 720
    Width = 1127
    Height = 57
    Align = alBottom
    BevelInner = bvLowered
    TabOrder = 1
    object btnClose: TButton
      Left = 1040
      Top = 14
      Width = 75
      Height = 25
      Caption = 'Close'
      TabOrder = 0
      OnClick = btnCloseClick
    end
  end
  object pnlPractitioner: TPanel
    Left = 0
    Top = 156
    Width = 1127
    Height = 564
    Align = alClient
    TabOrder = 2
    object Splitter1: TSplitter
      Left = 813
      Top = 1
      Height = 562
      Align = alRight
      ExplicitLeft = 808
      ExplicitTop = 344
      ExplicitHeight = 100
    end
    object DBGridPractitioner: TDBGrid
      Left = 1
      Top = 1
      Width = 812
      Height = 562
      Align = alClient
      DataSource = dmMain.dsPractitionerList
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnTitleClick = DBGridPractitionerTitleClick
      Columns = <
        item
          Expanded = False
          FieldName = 'PractId'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Name'
          Width = 64
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Given'
          Width = 64
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Prefix'
          Width = 64
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Active'
          Width = 64
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EMail'
          Title.Caption = 'E-Mail'
          Width = 64
          Visible = True
        end>
    end
    object Panel1: TPanel
      Left = 816
      Top = 1
      Width = 310
      Height = 562
      Align = alRight
      TabOrder = 1
      object lblPrefix: TLabel
        Left = 20
        Top = 23
        Width = 24
        Height = 13
        Caption = 'Title:'
      end
      object lblGiven: TLabel
        Left = 20
        Top = 46
        Width = 31
        Height = 13
        Caption = 'Name:'
      end
      object lblName: TLabel
        Left = 20
        Top = 69
        Width = 46
        Height = 13
        Caption = 'Surname:'
      end
      object lblPractId: TLabel
        Left = 20
        Top = 92
        Width = 41
        Height = 13
        Caption = 'FHIR Id:'
      end
      object lblEmail: TLabel
        Left = 20
        Top = 115
        Width = 74
        Height = 13
        Caption = 'E-Mail Address:'
      end
      object cbActive: TCheckBox
        Left = 20
        Top = 138
        Width = 97
        Height = 17
        Caption = 'Active'
        TabOrder = 0
      end
      object edtPrefix: TEdit
        Left = 144
        Top = 20
        Width = 50
        Height = 21
        TabOrder = 1
      end
      object edtGiven: TEdit
        Left = 144
        Top = 43
        Width = 160
        Height = 21
        TabOrder = 2
      end
      object edtName: TEdit
        Left = 144
        Top = 66
        Width = 160
        Height = 21
        TabOrder = 3
      end
      object edtPractId: TEdit
        Left = 144
        Top = 89
        Width = 160
        Height = 21
        TabOrder = 4
      end
      object edtMail: TEdit
        Left = 144
        Top = 112
        Width = 160
        Height = 21
        TabOrder = 5
      end
    end
  end
  object menuPractitioner: TMainMenu
    Left = 1088
    Top = 8
    object miFile: TMenuItem
      Caption = 'File'
      object Close1: TMenuItem
        Caption = 'Close'
        OnClick = btnCloseClick
      end
    end
    object miFHIRServer: TMenuItem
      Caption = 'FHIR Server'
      object PickServer1: TMenuItem
        Caption = 'Pick Server'
        OnClick = PickServer1Click
      end
    end
    object Practitioner1: TMenuItem
      Caption = 'Practitioner'
      object SearchPractitioner: TMenuItem
        Caption = 'Search'
        OnClick = btnSearchPractitionerClick
      end
      object SavePractitioner: TMenuItem
        Caption = 'Save'
        OnClick = SavePractitionerClick
      end
    end
    object Patient1: TMenuItem
      Caption = 'Patient'
      object SearchPatient: TMenuItem
        Caption = 'Search'
        OnClick = SearchPatientClick
      end
      object SavePatient: TMenuItem
        Caption = 'Save'
        OnClick = SavePatientClick
      end
    end
  end
  object SaveDialog1: TSaveDialog
    Left = 1088
    Top = 56
  end
end
