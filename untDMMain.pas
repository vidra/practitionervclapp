﻿unit untDMMain;

interface

uses
  System.SysUtils, System.Classes, IPPeerClient, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, REST.Response.Adapter, REST.Client, Data.Bind.Components,
  Data.Bind.ObjectScope
  , untPractitioner
  , untPatient
  , untObserverObjects
  , untErrorObject;

type
  TdmMain = class(TDataModule)
    RESTClientPractitioner: TRESTClient;
    RESTRequestPractitioner: TRESTRequest;
    RESTResponsePractitioner: TRESTResponse;
    RESTResponsePractitionerAdapter: TRESTResponseDataSetAdapter;
    memtblPractitioner: TFDMemTable;
    dsPractitioner: TDataSource;
    memtblPractitionerList: TFDMemTable;
    dsPractitionerList: TDataSource;
    memtblPractitionerListName: TStringField;
    memtblPractitionerListGiven: TStringField;
    memtblPractitionerListPrefix: TStringField;
    memtblPractitionerListActive: TBooleanField;
    memtblPractitionerListPractId: TStringField;
    memtblPractitionerListEMail: TStringField;
    RESTClientPatient: TRESTClient;
    RESTRequestPatient: TRESTRequest;
    RESTResponsePatient: TRESTResponse;
    memtblPatientList: TFDMemTable;
    dsPatientList: TDataSource;
    memtblPatientListName: TStringField;
    memtblPatientListGiven: TStringField;
    memtblPatientListPrefix: TStringField;
    memtblPatientListActive: TBooleanField;
    memtblPatientListPractId: TStringField;
    memtblPatientListEMail: TStringField;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure memtblPractitionerListAfterScroll(DataSet: TDataSet);
    procedure memtblPatientListAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    procedure FillPractitionerData(var MyPractitioner: TPractitioner; ResourceObject: untPractitioner.TResourceClass);
    procedure FillPatientData(var MyPatient: TPatient; ResourceObject: untPatient.TResourceClass);
    procedure ClearPractitionerListMemTable;
    procedure ClearPatientListMemTable;
  public
    { Public declarations }
    ObserverPractitionerServer: TPractitionerObserver;
    ObserverPatientServer: TPatientObserver;
    function GetPractitionerList(EMail, Family, Id: String; OnlyActive: Boolean; var mErrorObj: TErrorObject): Boolean; overload;
    function GetPatientList(EMail, Family, Id: String; OnlyActive: Boolean; var mErrorObj: TErrorObject): Boolean; overload;
    function GetPatientList(var mErrorObj: TErrorObject): Boolean; overload;
    function GetPractitionerList(var mErrorObj: TErrorObject): Boolean; overload;
    procedure FillPractitionerListMemTable(Practitioner: TPractitioner);
    procedure FillPatientListMemTable(Patient: TPatient);
    procedure CloseDataSources;
    function SetServerAddress(NewServer: String): Boolean;
    procedure SortGridListMemTable(FieldName: string; MemTable: TDataSource);
    function ReturnJsonTextFromResource(ResourceName:String): String;
    function GetWorkingServer: String;
  end;

var
  dmMain: TdmMain;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdmMain }

procedure TdmMain.ClearPatientListMemTable;
begin
  memtblPatientList.Close;
end;

procedure TdmMain.ClearPractitionerListMemTable;
begin
  memtblPractitionerList.Close;
end;

procedure TdmMain.CloseDataSources;
begin
  ClearPractitionerListMemTable;
  ClearPatientListMemTable;
end;

procedure TdmMain.FillPatientData(var MyPatient: TPatient;
  ResourceObject: untPatient.TResourceClass);
var
  Name: string; Given: string; EMail: String; Prefix: string; ID: string; Active: Boolean;
begin
  try
    try
      Name := ResourceObject.name[0].family;
    except
      on E: Exception do
        Name := '';
    end;
    try
      Given := ResourceObject.name[0].given[0];
    except
      on E: Exception do
        Given := '';
    end;

    Prefix := '';

    try
      ID := ResourceObject.id;
    except
      on E: Exception do
        ID := '';
    end;

        Active := True;

    try
      EMail := ''; // TODO telecom block
    except
      on E: Exception do
        Email := '';
    end;
  finally
    MyPatient.Name := Name;
    MyPatient.Given := Given;
    MyPatient.Mail := Prefix;
    MyPatient.PatientId := Id;
    MyPatient.Active := Active;
  end;

end;

procedure TdmMain.FillPatientListMemTable(Patient: TPatient);
begin
  if not memtblPatientList.Active then
    memtblPatientList.Open;

  if memtblPatientList.State <> dsInsert then
    memtblPatientList.Insert;

  memtblPatientList.FieldByName('name').AsString := Patient.Name;
  memtblPatientList.FieldByName('given').AsString := Patient.Given;
  memtblPatientList.FieldByName('prefix').AsString := Patient.Mail;
  memtblPatientList.FieldByName('PatientId').AsString := Patient.PatientId;
  memtblPatientList.FieldByName('Active').AsBoolean := Patient.Active;

  memtblPatientList.Post;
end;

procedure TdmMain.FillPractitionerData(var MyPractitioner: TPractitioner; ResourceObject: untPractitioner.TResourceClass);
var
  Name: string; Given: string; EMail: String; Prefix: string; ID: string; Active: Boolean;
begin
  try
    try
      Name := ResourceObject.name[0].family;
    except
      on E: Exception do
        Name := '';
    end;
    try
      Given := ResourceObject.name[0].given[0];
    except
      on E: Exception do
        Given := '';
    end;
    try
      Prefix := ResourceObject.name[0].prefix[0];
    except
      on E: Exception do
        Prefix := '';
    end;
    try
      ID := ResourceObject.id;
    except
      on E: Exception do
        ID := '';
    end;
    try
      Active := ResourceObject.active;
    except
      on E: Exception do
        Active := false;
    end;
    try
      EMail := ''; // TODO telecom block
    except
      on E: Exception do
        Email := '';
    end;
  finally
    MyPractitioner.Name := Name;
    MyPractitioner.Given := Given;
    MyPractitioner.Mail := Prefix;
    MyPractitioner.PractId := Id;
    MyPractitioner.Active := Active;
  end;
end;

procedure TdmMain.DataModuleCreate(Sender: TObject);
begin
  ObserverPractitionerServer := TPractitionerObserver.Create;
  ObserverPatientServer := TPatientObserver.Create;
end;

procedure TdmMain.DataModuleDestroy(Sender: TObject);
begin
  CloseDataSources;
  FreeAndNil(ObserverPatientServer);
  FreeAndNil(ObserverPractitionerServer);
end;

procedure TdmMain.FillPractitionerListMemTable(Practitioner: TPractitioner);
begin
  if not memtblPractitionerList.Active then
    memtblPractitionerList.Open;

  if memtblPractitionerList.State <> dsInsert then
    memtblPractitionerList.Insert;

  memtblPractitionerList.FieldByName('name').AsString := Practitioner.Name;
  memtblPractitionerList.FieldByName('given').AsString := Practitioner.Given;
  memtblPractitionerList.FieldByName('prefix').AsString := Practitioner.Mail;
  memtblPractitionerList.FieldByName('PractId').AsString := Practitioner.PractId;
  memtblPractitionerList.FieldByName('Active').AsBoolean := Practitioner.Active;

  memtblPractitionerList.Post;

end;

function TdmMain.GetPractitionerList(EMail, Family, Id: String;
  OnlyActive: Boolean; var mErrorObj: TErrorObject): Boolean;
begin
  try
    try
      memtblPractitionerList.DisableControls;

      mErrorObj.Operation := 'RESTClientPractitioner.Params';
      RESTClientPractitioner.Params.Clear;
      if OnlyActive then
      begin
        RESTClientPractitioner.Params.AddItem('active', 'true');
      end;
      if Trim(Family) <> '' then
      begin
        RESTClientPractitioner.Params.AddItem('family', Trim(Family));
      end;

      // RESTClientPractitioner.Params.AddItem('id', Trim(Id));
      // Delete resource to get all
      RESTRequestPractitioner.Resource := Trim(Id);

      if Trim(EMail) <> '' then
      begin
        RESTClientPractitioner.Params.AddItem('email', Trim(EMail));
      end;

      Result := GetPractitionerList(mErrorObj);
    except
      On E:Exception do
      begin
        Result := False;
        mErrorObj.Message := E.Message;
      end;
    end;
  finally
    memtblPractitionerList.EnableControls;
  end;
end;

function TdmMain.GetPatientList(EMail, Family, Id: String; OnlyActive: Boolean;
  var mErrorObj: TErrorObject): Boolean;
begin
  try
    try
      memtblPatientList.DisableControls;

      mErrorObj.Operation := 'RESTClientPractitioner.Params';
      RESTClientPatient.Params.Clear;
      if OnlyActive then
      begin
        RESTClientPatient.Params.AddItem('active', 'true');
      end;
      if Trim(Family) <> '' then
      begin
        RESTClientPatient.Params.AddItem('family', Trim(Family));
      end;

      // Delete resource to get all
      RESTRequestPatient.Resource := Trim(Id);

      if Trim(EMail) <> '' then
      begin
        RESTClientPatient.Params.AddItem('email', Trim(EMail));
      end;

      Result := GetPatientList(mErrorObj);
    except
      On E:Exception do
      begin
        Result := False;
        mErrorObj.Message := E.Message;
      end;
    end;
  finally
    memtblPatientList.EnableControls;
  end;
end;

function TdmMain.GetPatientList(var mErrorObj: TErrorObject): Boolean;
var
  RootObject: untPatient.TRootClass;
  ResourceObject: untPatient.TResourceClass;
  Name, Given, Prefix: String;
  ID: String;
  Active: Boolean;
  PatientItem: untPatient.TEntryClass;
  MyPatient: TPatient;
begin
  Result := True;
  ResourceObject := nil;
  RootObject := nil;
  MyPatient := nil;
  mErrorObj.Operation := 'ClearPatientTable';
  ClearPatientListMemTable;
  try
    try
      mErrorObj.Operation := 'RESTRequestPatient.Execute';
      RESTRequestPatient.Execute;

      mErrorObj.Operation := 'Check response for RESTRequestPatient.Execute error';

      if RESTResponsePatient.StatusCode <> 200 then
      begin
        Result := False;
        mErrorObj.Message := RESTResponsePractitioner.StatusText;
        Exit;
      end;

      mErrorObj.Operation := 'Serialize TRootClass Object';
      RootObject := untPatient.TRootClass.FromJsonString(RESTResponsePatient.JSONText);
      MyPatient := TPatient.Create;

      mErrorObj.Operation := 'ResourceObject from TEntryClass';
      if RootObject.entry = nil then
      begin
        mErrorObj.Operation := 'ResourceObject from TResourceClass';
        ResourceObject := untPatient.TResourceClass.FromJsonString(RESTResponsePatient.JSONText);
        if ResourceObject <> nil then
        begin
          if ResourceObject.resourceType = 'Patient' then
          begin
            FillPatientData(MyPatient, ResourceObject);
            FillPatientListMemTable(MyPatient);
          end;
        end;
      end else
      for PatientItem in RootObject.entry do
      begin
        FillPatientData(MyPatient, PatientItem.resource);
        FillPatientListMemTable(MyPatient);
      end;
      mErrorObj.Operation := 'FillPractitionerListMemTable done';
      Result := memtblPatientList.Active and (not memtblPatientList.IsEmpty);

      if Result then
        ObserverPatientServer.NotifyObeservers(MyPatient)
      else
        mErrorObj.Message := 'No data found';
    except
      On E:Exception do
      begin
        Result := False;
        mErrorObj.Message := E.Message;
      end;
    end;
  finally
    if MyPatient <> nil then MyPatient.Free;
    if RootObject <> nil then RootObject.Free;
    if ResourceObject <> nil then ResourceObject.Free;
    mErrorObj.Status := Result;
  end;

end;

function TdmMain.GetPractitionerList(var mErrorObj: TErrorObject): Boolean;
var
  RootObject: untPractitioner.TRootClass;
  ResourceObject: untPractitioner.TResourceClass;
  Name, Given, Prefix: String;
  ID: String;
  Active: Boolean;
  PractitionerItem: untPractitioner.TEntryClass;
  MyPractitioner: TPractitioner;
begin
  Result := True;
  ResourceObject := nil;
  RootObject := nil;
  MyPractitioner := nil;
  mErrorObj.Operation := 'ClearPractitionerTable';
  ClearPractitionerListMemTable;
  try
    try
      mErrorObj.Operation := 'RESTRequestPractitioner.Execute';
      RESTRequestPractitioner.Execute;

      mErrorObj.Operation := 'Check response for RESTRequestPractitioner.Execute error';

      if RESTResponsePractitioner.StatusCode <> 200 then
      begin
        Result := False;
        mErrorObj.Message := RESTResponsePractitioner.StatusText;
        Exit;
      end;

      mErrorObj.Operation := 'Serialize TRootClass Object';
      RootObject := untPractitioner.TRootClass.FromJsonString(RESTResponsePractitioner.JSONText);
      MyPractitioner := TPractitioner.Create;

      mErrorObj.Operation := 'ResourceObject from TEntryClass';
      if RootObject.entry = nil then
      begin
        mErrorObj.Operation := 'ResourceObject from TResourceClass';
        ResourceObject := untPractitioner.TResourceClass.FromJsonString(RESTResponsePractitioner.JSONText);
        if ResourceObject <> nil then
        begin
          if ResourceObject.resourceType = 'Practitioner' then
          begin
            FillPractitionerData(MyPractitioner, ResourceObject);
            FillPractitionerListMemTable(MyPractitioner);
          end;
        end;
      end else
      for PractitionerItem in RootObject.entry do
      begin
        FillPractitionerData(MyPractitioner, PractitionerItem.resource);
        FillPractitionerListMemTable(MyPractitioner);
      end;
      mErrorObj.Operation := 'FillPractitionerListMemTable done';
      Result := memtblPractitionerList.Active and (not memtblPractitionerList.IsEmpty);

      if Result then
        ObserverPractitionerServer.NotifyObeservers(MyPractitioner)
      else
        mErrorObj.Message := 'No data found';
    except
      On E:Exception do
      begin
        Result := False;
        mErrorObj.Message := E.Message;
      end;
    end;
  finally
    if MyPractitioner <> nil then MyPractitioner.Free;
    if RootObject <> nil then RootObject.Free;
    if ResourceObject <> nil then ResourceObject.Free;
    mErrorObj.Status := Result;
  end;
end;

function TdmMain.GetWorkingServer: String;
begin
  Result := Copy(dmMain.RESTClientPractitioner.BaseURL, 1, Pos('/Pract', dmMain.RESTClientPractitioner.BaseURL));
end;

procedure TdmMain.memtblPatientListAfterScroll(DataSet: TDataSet);
var
  NewPatient: TPatient;
begin
  NewPatient := TPatient.Create;

  NewPatient.Name := DMMain.memtblPatientListName.AsString;
  NewPatient.Given := DMMain.memtblPatientListGiven.AsString;
  NewPatient.Mail := DMMain.memtblPatientListPrefix.AsString;
  NewPatient.PatientId := DMMain.memtblPatientListPractId.AsString;
  NewPatient.Active := DMMain.memtblPatientListActive.AsBoolean;

  ObserverPatientServer.NotifyObeservers(NewPatient);

  FreeAndNil(NewPatient);

end;

procedure TdmMain.memtblPractitionerListAfterScroll(DataSet: TDataSet);
var
  NewPractitioner: TPractitioner;
begin
  NewPractitioner := TPractitioner.Create;

  NewPractitioner.Name := DMMain.memtblPractitionerListName.AsString;
  NewPractitioner.Given := DMMain.memtblPractitionerListGiven.AsString;
  NewPractitioner.Mail := DMMain.memtblPractitionerListPrefix.AsString;
  NewPractitioner.PractId := DMMain.memtblPractitionerListPractId.AsString;
  NewPractitioner.Active := DMMain.memtblPractitionerListActive.AsBoolean;

  ObserverPractitionerServer.NotifyObeservers(NewPractitioner);

  FreeAndNil(NewPractitioner);

end;

function TdmMain.ReturnJsonTextFromResource(ResourceName: String): String;
begin
  Result := ResourceName + ' is not supported Resource for ReturnJsonTextFromResource';
  if ResourceName = 'Practitoner' then
    Result := dmMain.RESTResponsePractitioner.JSONText;
  if ResourceName = 'Patient' then
    Result := dmMain.RESTResponsePatient.JSONText;
end;

function TdmMain.SetServerAddress(NewServer: String): Boolean;
begin
  CloseDataSources;
  // http://test.fhir.org/r3/Practitioner
  RESTClientPractitioner.BaseURL := NewServer + 'Practitioner';
  RESTClientPatient.BaseURL := NewServer + 'Patient';
  RESTRequestPractitioner.Resource := '';
  RESTRequestPractitioner.Resource := '';
end;

procedure TdmMain.SortGridListMemTable(FieldName: string; MemTable: TDataSource);
begin
  if Pos(FieldName, (MemTable.DataSet as TFDMemTable).IndexFieldNames) > 0 then
  begin
    if Pos(':A', (MemTable.DataSet as TFDMemTable).IndexFieldNames) > 0 then
      (MemTable.DataSet as TFDMemTable).IndexFieldNames := FieldName + ':D'
    else (MemTable.DataSet as TFDMemTable).IndexFieldNames := FieldName + ':A';
  end else
    (MemTable.DataSet as TFDMemTable).IndexFieldNames := FieldName + ':A';
end;

end.
