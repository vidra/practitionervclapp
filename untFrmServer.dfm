object FrmPickServer: TFrmPickServer
  Left = 0
  Top = 0
  Caption = 'Select FHIR server'
  ClientHeight = 107
  ClientWidth = 313
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlControls: TPanel
    Left = 0
    Top = 0
    Width = 313
    Height = 66
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 352
    ExplicitTop = 24
    ExplicitWidth = 185
    ExplicitHeight = 41
    object lblServerName: TLabel
      Left = 15
      Top = 25
      Width = 98
      Height = 13
      Caption = 'Pick Server from list:'
    end
    object cbServerList: TComboBox
      Left = 119
      Top = 22
      Width = 177
      Height = 21
      TabOrder = 0
      OnExit = cbServerListExit
      Items.Strings = (
        'http://hapi.fhir.org/baseR4/'
        'http://test.fhir.org/r3/'
        'https://test.fhir.org/r3/'
        'http://test.fhir.org/r4/'
        'https://test.fhir.org/r4/')
    end
  end
  object pnlOperations: TPanel
    Left = 0
    Top = 66
    Width = 313
    Height = 41
    Align = alBottom
    TabOrder = 1
    ExplicitLeft = 256
    ExplicitTop = 248
    ExplicitWidth = 185
    DesignSize = (
      313
      41)
    object btnCancel: TButton
      Left = 218
      Top = 9
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Cancel'
      TabOrder = 0
      OnClick = btnCancelClick
      ExplicitLeft = 553
    end
    object btnSave: TButton
      Left = 114
      Top = 9
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Save'
      TabOrder = 1
      OnClick = btnSaveClick
      ExplicitLeft = 449
    end
  end
end
